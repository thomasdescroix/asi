// default.route.js
const express = require('express');
const router = express.Router();

router.route('/')
    .get( (req, res) => {
//        res.send('It works');
        res.sendFile(__dirname + '/views/index.html');
    });

module.exports = router;
