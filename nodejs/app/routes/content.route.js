const express = require('express');
const router = express.Router();
const contentController = require("./../controllers/content.controller");
const multer = require('multer');
const multerMiddleware = multer({ "dest": "/tmp/" });

router.route("/contents")
    .get(contentController.list)
    .post(multerMiddleware.single('file'), contentController.create);

router.route("/contents/:contentId")
    .get(contentController.read);

router.param("contentId", (req, res, next, id) => {
    req.contentId = id;
    next();
});

module.exports = router;
