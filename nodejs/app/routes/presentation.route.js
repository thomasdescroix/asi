// presentation.route.js
const express = require('express');
const router = express.Router();
const PresentationController = require("./../controllers/presentation.controller");

router
  .get('/loadPres', PresentationController.load)
  .post('/savePres', PresentationController.save);

module.exports = router;
