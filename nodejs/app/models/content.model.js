const fs = require('fs');
const utils = require('../utils/utils');

class ContentModel {
    /** @constructor */
    constructor({ type, id, title, src, fileName } = {}){
        /** @public */
        this.id = id;
        /** @public */
        this.type = type;
        /** @public */
        this.title = title;
        /** @public */
        this.src = src;
        /** @public */
        this.fileName = fileName;

        /** @private */
        let data;
        this.getData = () => data;
        this.setData = (newData) => {
            data = newData;
        };
    }

    /**
     * Stores asynchronously the [contentModel.data] in a [contentModel.fileName] file and
     * stores asynchronously the metadata in a [contentModel.id].meta.json file in
     * [CONFIG.contentDirectory] directory.
     *
     * @param {Object} content - The ContentModel object
     * @param {function} callback - The callback that handles the response
     */
    static create(content, callback) {
        let success = 0;

        if (content && content.fileName && content.id) {
            fs.writeFile(utils.getDataFilePath(content.fileName), content.getData(), (err) => {
                if (err) return callback(err);

                success += 1;
                if (success === 2) {
                    return callback();
                }
            });

            fs.writeFile(utils.getMetaFilePath(content.id), JSON.stringify(content), (err) => {
                if (err) return callback(err);

                success +=1;
                if (success === 2) {
                    return callback();
                }
            });
        } else {
            const err = new Error('Create failed: missing data!');
            err.statusCode = 500;
            return callback(err);
        }
    }

    /**
     * Returns a contentModel object reading asynchronously from [id].meta.json file.
     *
     * @param {number} id - The id of contentModel
     * @param {function} callback - The callback that handles the response
     */
    static read(id, callback) {
        utils.readFileIfExists(utils.getMetaFilePath(id), (err, data) => {
            if (err) return callback(err);
            return callback(null, new ContentModel(JSON.parse(data)));
        });
    }

    /**
     * Updates an existing metadata file ([content.id].meta.json) with the specified
     * contentModel. The [content.fileName] file is updated only if [content.type]
     * is equal to 'img' and if [content.data] is not null and his length is higher than 0.
     *
     * @param {Object} content - The ContentModel object
     * @param {function} callback - The callback that handles the response
     */
    static update(content, callback) {
        if (content && content.id) {
            utils.fileExists(utils.getMetaFilePath(content.id), (err) => {
                if (err) return callback(err);

                fs.writeFile(utils.getMetaFilePath(content.id), JSON.stringify(content), (err) => {
                    if (err) return callback(err);

                    if (content.type === 'img' && content.getData().length > 0) {
                        fs.writeFile(utils.getDataFilePath(content.fileName), content.getData(), (err) => {
                            if (err) return callback(err);
                            return callback();
                        });
                    }
                });
            });
        } else {
            const err = new Error('Update failed: missing data!');
            err.statusCode = 500;
            return callback(err);
        }
    }

    /**
     * Deletes the [content.fileName] and [content.id].meta.json files with the specified id.
     *
     * @param {number} id - The id of contentModel
     * @param {function} callback - The callback that handles the response
     */
    static delete(id, callback) {
        let success = 0;

        utils.readFileIfExists(utils.getMetaFilePath(id), (err, file) => {
            if (err) { return callback(err); }

            fs.unlink(utils.getDataFilePath(JSON.parse(file).fileName), (err) => {
                if (err) return callback(err);

                success += 1;
                if (success === 2) {
                    return callback();
                }
            });

            fs.unlink(utils.getMetaFilePath(id), (err) => {
                if (err) return callback(err);

                success += 1;
                if (success === 2) {
                    return callback();
                }
            });
        });
    }
}

module.exports = ContentModel;