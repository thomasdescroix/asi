const fs = require('fs');
const path = require('path');
const CONFIG = JSON.parse(process.env.CONFIG);

this.generateUUID = () => {
    let d = new Date().getTime();
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        const r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

this.fileExists = (path, callback) => {
    fs.stat(path, (err, stat) => {
        if (err) {
            callback(err);
        } else {
            if (stat.isFile()) {
                callback(null);
            }
        }
    });
};

this.readFileIfExists = (path, callback) => {
    this.fileExists(path, (err) => {
        if (err) {
            callback(err);
        } else {
            fs.readFile(path, callback);
        }
    });
};

this.getMetaFilePath = (id) => {
    return path.join(CONFIG.contentDirectory, id + ".meta.json");
};

this.getDataFilePath = (fileName) => {
    return path.join(CONFIG.contentDirectory, fileName);
};

this.getNewFileName = (id, originalFileName) => {
    return id + '.' + originalFileName.split('.').pop();
};

module.exports = this;