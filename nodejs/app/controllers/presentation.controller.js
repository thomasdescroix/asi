const CONFIG = JSON.parse(process.env.CONFIG);
const dirname = CONFIG.presentationDirectory;
const path = require('path');
const fs = require('fs');

const PresentationController = {

    load(req, res) {
      fs.readdir(dirname, (err, files) => {
          const jsonResult = {};
          const jsonFiles = files.filter( file => path.extname(file) === '.json' );
          jsonFiles.forEach( (file, index) => {
              fs.readFile(path.join(dirname, "/", file), 'utf8', (err, content) => {
                  const jsonData = JSON.parse(content);
                  jsonResult[jsonData.id] = jsonData;

                  if(index+1 === jsonFiles.length) {
                      res.json(jsonResult);
                  }
              });
          });

      });
    },

    save(req, res) {
      fs.writeFile(path.join(dirname, "/", req.body.id + ".json"), JSON.stringify(req.body), (err) => {
          if (err) throw err;
          res.json(req.body);
      });
    }
};

module.exports = PresentationController;
