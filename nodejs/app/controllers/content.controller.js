const CONFIG = JSON.parse(process.env.CONFIG);
const dirname = CONFIG.contentDirectory;
const path = require('path');
const fs = require('fs');
const ContentModel = require("./../models/content.model");
const utils = require('../utils/utils');

const ContentController = {
    /**
     * Returns all content files in a JSON object
     */
    list(req, res) {
        fs.readdir(dirname, (err, files) => {
            const jsonResult = {};
            const jsonFiles = files.filter( file => path.extname(file) === '.json');
            jsonFiles.forEach( (file, index) => {
                fs.readFile(path.join(dirname, "/", file), 'utf8', (err, content) => {
                    if (err) return next(err);

                    const jsonData = JSON.parse(content);
                    jsonResult[jsonData.id] = jsonData;

                    if (index+1 === jsonFiles.length) {
                        res.json(jsonResult);
                    }
                })
            });
        });
    },
    /**
     *
     */
    create(req, res, next) {
        const content = new ContentModel();
        content.id = utils.generateUUID();
        content.fileName = req.body.fileName;
        content.title = req.body.title;
        content.type = req.body.type;

        if (content.type !== 'img') {
            content.src = null;
            ContentModel.create(content, (err) => {
               if (err) return next(err);
            });
        } else {
            content.src = req.body.src;
            // TODO: upload file
            ContentModel.create(content, (err) => {
                if (err) return next(err);
            });
        }
        res.json(content);
    },
    /**
     *
     */
    read(req, res, next) {
        ContentModel.read(req.params.contentId, (err, data) => {
            if (err) return next(err);

            if (data.type === 'img') {
                res.sendFile(path.join(__dirname, "../../", dirname, data.fileName), (err) => {
                    if (err) return next(err);
                });
            } else if (req.query.json === "true") {
                res.json(data);
            } else if (!data.fileName) {
                res.redirect(data.src);
            }
        });
    }
};

module.exports = ContentController;