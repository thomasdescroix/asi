const CONFIG = JSON.parse(process.env.CONFIG);
const socketio = require('socket.io');
const http = require('http');
const ContentModel = require('../models/content.model');

const IOController = {

  listen(httpServer) {
    const io = socketio(httpServer);
    const socketMap = new Map();

    io.on('connection', (socket) => {
      socket.emit('connection');

      socket.on('_data_comm_', (socketID) => {
        socketMap.set(socketID, socket);
      });

      socket.on('slidEvent', (message) => {
        switch(message.CMD) {
          case "START": {
            console.log("START");
            break;
          }
          case "END": {
            console.log("END");
            break;
          }
          case "NEXT": {
            console.log("NEXT");
            break;
          }
          case "PREV": {
            console.log("PREV");
            break;
          }
          default: {
            break;
          }
        }
        // Pour les commandes START, END, BEGIN, PREV et NEXT, on
        //récupère et on envoie
        // les métadonnées du contenu de la slide que l'on doit
        //diffuser à toutes les sockets
        // connectées (penser à passer par ContentModel pour lire
        //les métadonnées).
      });

    });


  }
};

module.exports = IOController;
