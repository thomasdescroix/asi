// app.js
const express = require('express');
const app = express();
const http = require('http');
const CONFIG = require('./config.json');
process.env.CONFIG = JSON.stringify(CONFIG);
const server = http.createServer(app);
const defaultRoute = require('./app/routes/default.route');
const presentationRoute = require('./app/routes/presentation.route');
const contentRoute = require('./app/routes/content.route');
const authRoute = require('./app/routes/auth.route');
const IOController = require('./app/controllers/io.controller');
const path = require('path');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(defaultRoute);
app.use(presentationRoute);
app.use(contentRoute);
app.use(authRoute);
app.use("/admin", express.static(path.join(__dirname, "public/admin")));
app.use("/watch", express.static(path.join(__dirname, "public/watch")));

server.listen(CONFIG.port, () => {
    console.log(`listening on ${CONFIG.port}`);
    IOController.listen(server);
});
