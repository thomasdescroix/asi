import React, { Component } from 'react';
import Slid from '../../common/slid/containers/Slid';

import {connect } from 'react-redux';

class EditSlidPanel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const slid = this.props.contentMap[Object.keys(this.props.contentMap)[1]];
    console.log(slid);
    return (

      <div>
        <Slid slid={slid} displayMode="FULL_MNG" />
      </div>
    );
  }

}

const mapStateToProps = (state, ownProps) => {
  return {
    selected_slid: state.selectedReducer.slid,
  }
};

export default connect(mapStateToProps)(EditSlidPanel);
