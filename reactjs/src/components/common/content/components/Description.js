import React, { Component } from 'react';

class Description extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul className="list-inline">
        <li className="list-inline-item">{this.props.id}</li>
        <li className="list-inline-item">{this.props.title}</li>
      </ul>
    );
  }
}

export default Description;
