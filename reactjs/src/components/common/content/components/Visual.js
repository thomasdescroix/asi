import React, { Component } from 'react';

class Visual extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // TODO img base 64
    let render_visual;

    switch(this.props.type) {
      case "img_url": {
        render_visual= (
          <img
            className="mx-auto d-block mw-100"
            src={this.props.src}
            alt={this.props.title}
          />
        );
        break;
      }
      case "video": {
        render_visual= (
          <div className="embed-responsive embed-responsive-16by9 mw-100">
            <iframe className="embed-responsive-item" src={this.props.src} allowFullScreen></iframe>
          </div>
        );
        break;
      }
      case "web": {
        render_visual= (
          <div className="embed-responsive embed-responsive-21by9 mw-100">
            <iframe className="embed-responsive-item" src={this.props.src}></iframe>
          </div>
        );
        break;
      }
      default: {
        render_visual= null;
        console.error("File not load!");
        break;
      }
    }

    return (
      <div>
        {render_visual}
      </div>
    );
  }
}

export default Visual;
