import React, { Component } from 'react';
import Visual from '../components/Visual';
import Description from '../components/Description';

class Content extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const onlyContent = this.props.onlyContent;
    return (
      <div className="border mb-md-4">
        <Visual
          type={this.props.content.type}
          title={this.props.content.title}
          src={this.props.content.src}
        />
        { !onlyContent && <Description id={this.props.content.id} title={this.props.content.title} />}
      </div>
    );
  }
}

export default Content;
