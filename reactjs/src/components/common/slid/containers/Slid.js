import React, { Component } from 'react';
import './slid.css';
import Content from '../../content/containers/Content';
import EditMetaSlid from '../components/EditMetaSlid';
import { connect } from 'react-redux';
import {setSelectedSlid} from '../../../../actions';

class Slid extends Component {
  constructor(props) {
    super(props);
    this.updateSelectedSlid = this.updateSelectedSlid.bind(this);
  }

  updateSelectedSlid() {
    const tmpSlid = {
      id: this.props.id,
      title: this.props.title,
      txt: this.props.txt,
      content_id: this.props.content_id
    };
    this.props.dispatch(setSelectedSlid(tmpSlid));
  }

  render() {
    return (

      <div>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">{this.props.slid.title}</h5>
            <p className="card-text">{this.props.slid.txt}</p>
          </div>
          <Content content={this.props.slid} onlyContent />
        </div>
        { (this.props.displayMode === 'FULL_MNG') && <EditMetaSlid title={this.props.slid.title} txt={this.props.slid.txt} />}
      </div>
    );
  }
}

export default connect()(Slid);
