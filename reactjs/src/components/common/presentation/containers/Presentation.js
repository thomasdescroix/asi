import React, { Component } from 'react';
import './presentation.css';
import EditMetaPres from '../components/EditMetaPres';
import Slid from '../../slid/containers/Slid';

class Presentation extends Component {
  constructor(props) {
    super(props);
  }

  getAllSlids(presentation) {
    return Object.keys(presentation).map((id, index) => {
      return (<Slid
        key={index}
        slid={presentation[id]}
        displayMode='SHORT'
      />);
    });
  }


  render() {
    const render_slids= this.getAllSlids(this.props.presentation.slidArray);
    return (
      <div>
        <EditMetaPres title={this.props.presentation.title} description={this.props.presentation.description} />
        {render_slids}
      </div>
    );
  }
}

export default Presentation;
