import React, {Component} from 'react';

class EditMetaPres extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (

      <div className="form-group">
        <label htmlFor="currentPresentationTitle">Title </label>
        <input
          type="text"
          className="form-control"
          id="currentPresentationTitle"
          onChange={this.props.handleChangeTitle}
          value={this.props.title}
        />

        <label htmlFor="currentPresentationDescription">Description </label>
        <textarea
          rows="5"
          type="text"
          className="form-control"
          id="currentPresentationDescription"
          onChange={this.props.handleChangeDescription}
          value={this.props.description}>
        </textarea>
      </div>
    );
  }
}

 export default EditMetaPres;
