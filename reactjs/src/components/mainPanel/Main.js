import React, { Component } from 'react';
import BrowseContentPanel from '../browseContentPanel/containers/BrowseContentPanel';
import BrowsePresentationPanel from '../browsePresentationPanel/containers/BrowsePresentationPanel';
import EditSlidPanel from '../editSlidPanel/containers/EditSlidPanel';

import './main.css';
import '../../lib/bootstrap-4.1.3/css/bootstrap.min.css';
import * as contentMapTmp from '../../source/contentMap.json';
import * as presTmp from '../../source/pres.json';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import globalReducer from '../../reducers';

const store = createStore(globalReducer);

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contentMap:contentMapTmp.default,
      pres:presTmp.default
    }
  }

  render() {
    return (

      <Provider store={store} >
        <div className="container-fluid h-100">
          <div className="row h-100">
            <div className="col-md h-100 vertical-scroll">
              <BrowsePresentationPanel presentation={this.state.pres} />
            </div>
            <div className="col-md-6 h-100">
              <EditSlidPanel selected_slid={1} contentMap={this.state.contentMap} />
            </div>
            <div className="col-md h-100">
              <BrowseContentPanel contents={this.state.contentMap} />
            </div>
          </div>
        </div>
      </Provider>
    );
  }
}
