import React, { Component } from 'react';
import Presentation from '../../common/presentation/containers/Presentation';

class BrowsePresentationPanel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Presentation presentation={this.props.presentation} />
    );
  }
}

export default BrowsePresentationPanel;
