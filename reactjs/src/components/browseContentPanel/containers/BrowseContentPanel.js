import React, { Component } from 'react';
import Content from '../../common/content/containers/Content';

class BrowseContentPanel extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return Object.keys(this.props.contents).map((id, index) => {
      return (
        <Content
          key={index}
          content={this.props.contents[id]}
          onlyContent={false}
        />
      );
    });
  }
}

export default BrowseContentPanel;
