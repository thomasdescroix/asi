package fr.cpe.dto;

import java.io.Serializable;

/**
 * Objet d'entréé - requête d'identification
 */
public class AuthentificationRequest implements Serializable {
    public AuthentificationRequest(){

    }

    public AuthentificationRequest(String login, String pwd) {
        this.login = login;
        this.pwd = pwd;
    }

    public String getLogin() {
        return login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    private String login;
    private String pwd;

    @Override
    public String toString() {
        return this.getLogin() + " " + this.getPwd();
    }
}
