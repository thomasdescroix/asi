package fr.cpe.dto;

import fr.cpe.model.User;

import java.io.Serializable;

public class UserTransport implements Serializable {

    public UserTransport() {
        this.login = null;
        this.role = null;
        this.validAuth = false;
    }

    public UserTransport(User user) {
        this.login = user.getLogin();
        this.role = user.getRole();
        this.validAuth = false;
    }

    public String getLogin() {
        return this.login;
    }

    public String getRole() {
        return this.role;
    }

    public boolean getValidAuth() {
        return this.validAuth;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setValidAuth(boolean validAuth) {
        this.validAuth = validAuth;
    }

    public boolean isValidAuth() {
        return validAuth;
    }

    @Override
    public String toString() {
        return this.getLogin() + " : " + this.getRole();
    }


    private String login;
    private String role;
    private boolean validAuth = false;
}
