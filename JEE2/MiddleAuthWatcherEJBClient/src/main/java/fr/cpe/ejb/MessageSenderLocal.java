package fr.cpe.ejb;

import fr.cpe.model.User;

import javax.ejb.Local;

@Local
public interface MessageSenderLocal {
    void sendMessage(String message);
    void sendMessage(User user);
}
