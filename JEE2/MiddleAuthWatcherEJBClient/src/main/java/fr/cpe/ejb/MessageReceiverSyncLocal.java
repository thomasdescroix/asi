package fr.cpe.ejb;

import fr.cpe.model.User;

import javax.ejb.Local;

@Local
public interface MessageReceiverSyncLocal {
    User receiveMessage();
}
