package fr.cpe.ejb;


import fr.cpe.data.UserRepository;
import fr.cpe.model.User;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;
import javax.persistence.NoResultException;
import java.util.logging.Logger;

@MessageDriven(/*name="messageReceiverSync",*/ activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "watcherAuth"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
})
public class AuthWatcherMsgDrivenEJB implements MessageListener {

    private final static Logger log = Logger.getLogger(AuthWatcherMsgDrivenEJB.class.getSimpleName());

    @Inject
    private UserRepository userRepository;
    
    @Override
    public void onMessage(Message message) {
        try{
            if(message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                logger.info(textMessage.getText());
            }
            else if(message instanceof ObjectMessage) {
                ObjectMessage objectMessage = (ObjectMessage) message;
                if(objectMessage.getObject() instanceof User) {
                    User user = (User) objectMessage.getObject();

                    User userBd = this.checkUser(user);
                    if(userBd != null) {
                        sender.sendMessage(user);
                    }
                }
            }
            else { logger.warning("message de type inconnu dans la queue"); }
        }
        catch (JMSException e) { logger.warning(e.getMessage()); }
    }

    private User checkUser(User user) {
        try {
            User userBd = userRepository.findByLogin(user.getLogin());
            if (!userBd.getPwd().equals(user.getPwd())) {
                log.warning("Wrong password");
                return null;
            }
            return userBd;

        } catch (NoResultException e) {
            log.warning("User not found in DB");
            return null;
        }

    }

    @EJB
    private MessageSenderQueueLocal sender;

    private Logger logger = Logger.getLogger(AuthWatcherMsgDrivenEJB.class.getSimpleName());
}
