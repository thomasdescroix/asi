package fr.cpe.rest.impl;

import fr.cpe.dto.AuthentificationRequest;
import fr.cpe.dto.UserTransport;
import fr.cpe.ejb.MessageReceiverSyncLocal;
import fr.cpe.ejb.MessageSenderLocal;
import fr.cpe.model.User;
import fr.cpe.rest.AuthRestService;

import javax.ejb.EJB;
import java.util.logging.Logger;

import static java.util.Optional.ofNullable;

public class AuthRestServiceImpl implements AuthRestService {

    @Override
    public UserTransport authenticate(AuthentificationRequest authRequest) {
        User user = new User(authRequest.getLogin(), authRequest.getPwd(), "ADMIN");
        messageSenderLocal.sendMessage(user);
        logger.info("sent message from WebService");
        User response = messageReceiverSyncLocal.receiveMessage();
        logger.info("received message from WebService");
        return ofNullable(response).map(UserTransport::new).orElse(null);
    }

    @Override
    public String displayMessage(String message) {
        return "test message";
    }

    @EJB
    private MessageSenderLocal messageSenderLocal;

    @EJB
    private MessageReceiverSyncLocal messageReceiverSyncLocal;

    private static Logger logger = Logger.getLogger(AuthRestServiceImpl.class.getSimpleName());
}
