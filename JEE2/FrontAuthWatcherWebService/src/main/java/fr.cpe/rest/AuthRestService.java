package fr.cpe.rest;

import fr.cpe.dto.AuthentificationRequest;
import fr.cpe.dto.UserTransport;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("watcherauth")
public interface AuthRestService {
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    UserTransport authenticate(AuthentificationRequest json);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    String displayMessage(String message);
}