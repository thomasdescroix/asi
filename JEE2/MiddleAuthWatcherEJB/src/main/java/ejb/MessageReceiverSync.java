package ejb;

import fr.cpe.ejb.MessageReceiverSyncLocal;
import fr.cpe.model.User;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Logger;

@Stateless
public class MessageReceiverSync implements MessageReceiverSyncLocal {
    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/watcherAuthJMS")
    Queue queue;

    @Override
    public User receiveMessage() {
        User user = null;
        logger.info(">>> receiveMessage() BEGIN " + queue);
        Message message = context.createConsumer(queue).receive(10000);
        if(message instanceof ObjectMessage) {
            try {
                user = (User) ((ObjectMessage)message).getObject();
            }
            catch (JMSException e) { logger.warning(e.getMessage());}
        }
        //context.createConsumer(queue).receive();
        logger.info(">>> receiveMessage() END " + queue);
        return user;
    }

    private final static Logger logger = Logger.getLogger(MessageReceiverSync.class.getSimpleName());
}
