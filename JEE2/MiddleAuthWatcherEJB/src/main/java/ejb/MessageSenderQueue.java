package ejb;

import fr.cpe.ejb.MessageSenderQueueLocal;
import fr.cpe.model.User;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import java.util.logging.Logger;

@Stateless
public class MessageSenderQueue implements MessageSenderQueueLocal {

    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/watcherAuthJMS")
    Queue queue;

    @Override
    public void sendMessage(String message) {
        context.createProducer().send(queue, message);
    }

    @Override
    public void sendMessage(User user) {
        //try{
            //ObjectMessage message = context.createObjectMessage();
            //message.setObject(user);
            logger.info("sent user in queue user:" + user.toString());
            context.createProducer().send(queue, user);
            logger.info("sent user in queue " + queue);
        //}
        //catch (JMSException e) { logger.warning(e.getMessage()); }
    }

    private Logger logger = Logger.getLogger(MessageSenderQueue.class.getSimpleName());
}
