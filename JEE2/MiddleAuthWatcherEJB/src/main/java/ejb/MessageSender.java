package ejb;

import fr.cpe.ejb.MessageSenderLocal;
import fr.cpe.model.User;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Topic;
import java.util.logging.Logger;

@Stateless
@LocalBean
//@LocalBean /* pas nécessaire, car action par défaut */
public class MessageSender implements MessageSenderLocal {
    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/watcherAuth")
    Topic topic;

    public void sendMessage(String message) {
        context.createProducer().send(topic, message);
    }

    public void sendMessage(User user) {
        context.createProducer().send(topic, user);
    }

    private final static Logger logger = Logger.getLogger(MessageSender.class.getSimpleName());
}
